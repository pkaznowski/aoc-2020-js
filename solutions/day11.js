const h = require("../tools/helpers");

const CMB = h.comb([-1, 0, 1], 2).filter((e) => !e.every((v) => v == 0));

const ss = (i, occupiedCounter, limit = 4) => {
  let gg = {};
  i.forEach((l, y) => {
    l.forEach((ch, x) => {
      gg[[x, y]] = { x: x, y: y, empty: +(ch == "L"), floor: ch == "." };
    });
  });

  // check state using occupied seats counter method
  const cs = (s) => {
    let occ = occupiedCounter(s, gg);
    let changed = false;

    if      (s.empty && occ == 0)      { s.next = 0; changed = true; } 
    else if (!s.empty && occ >= limit) { s.next = 1; changed = true; }

    return changed;
  };

  while (true) {
    let ch = [];
    Object.values(gg).forEach((v) => { if (!v.floor) ch.push(cs(v)); });
    if (!ch.some((e) => e)) break;
    Object.keys(gg).forEach((k, v) => { if (!gg[k].floor) gg[k].empty = gg[k].next ? 1 : 0; });
  }

  return Object.values(gg).map((v) => !v.floor ? v.empty ? 0 : 1 : 0).reduce(h.add)
};

const part1 = (input) => {
  const ii = h.asLines(input).map((l) => l.split(""));

  // get occupied neighbors
  const gn = ({ x, y }, oo) => 
    CMB.map(([xx, yy]) => [xx + x, yy + y])
      .map((k) => (k in oo && !oo[k].floor) ? oo[k].empty ? 0 : 1 : 0)
      .reduce(h.add);

  return ss(ii, gn);
};

const part2 = (input) => {
  const ii = h.asLines(input).map((l) => l.split(""));
  const [XX, YY] = [ii[0].length, ii.length];

  // get seen occupied seats
  const gn = ({ x, y }, o) => {
    const dirs = (xx, yy) => {
      let [dx, dy] = [x + xx, y + yy];
      while (dy >= 0 && dy < YY && dx >= 0 && dx < XX) {
        if (o[[dx, dy]].floor) { dy = dy + yy; dx = dx + xx; continue; }
        return !o[[dx, dy]].empty ? 1 : 0;
      } return 0; };
    return CMB.map(([xx, yy]) => dirs(xx, yy)).reduce(h.add);
  };

  return ss(ii, gn, 5);
};

module.exports = [part1, part2];
