const h = require("../tools/helpers");

const ss = (inp, crd) => {
  // initial space state (collect only active pockets)
  let sp = {};
  inp.forEach((l, y) => {
    l.split("").forEach((ch, x) => {
      if (ch == "#") {
        const k = crd.length == 3 ? [x, y, 0] : [x, y, 0, 0];
        sp[k] = { s: 1, x: x, y: y, z: 0, w: 0, n: 0 };
      }
    });
  });

  // neighbor coordinate combinations
  const CMB = h
    .comb([-1, 0, 1], crd.length)
    .filter((e) => !e.every((v) => v == 0));

  // get neighbors
  const gn = (cc) =>
    CMB.reduce(
      (aa, e) => [
        ...aa,
        e.reduce(([i, a], v) => [i + 1, [...a, v + cc[i]]], [0, []])[1],
      ],
      []
    );

  // get new pocket
  const np = (s) => {
    const n = s.split(",").map((n) => parseInt(n, 10));
    return [...crd].reduce(
      ([i, o], k) => {
        o[k] = n[i];
        return [i + 1, o];
      },
      [0, {}]
    )[1];
  };

  // check state: get values for the next turn
  const cs = (pp) => {
    let tt = sp[pp] ? sp[pp] : { s: 0, ...np(pp) };
    // count active neighbors and find new ones
    const [c, nn] = gn([tt.x, tt.y, tt.z, tt.w]).reduce(
      ([c, a], p) => (p in sp ? [c + sp[p].s, a] : [c, [...a, p]]),
      [0, []]
    );
    // save state for the next turn
    sp[pp] = { ...tt, n: (tt.s && [2, 3].includes(c)) || (!tt.s && c == 3) };
    // we need to get values for the discovered pockets as well -> this will
    // be the second go in the final part
    return nn;
  };

  // get result
  for (let i = 0; i < 6; i++) {
    // get state for next turn
    for (let k of Object.keys(sp)) cs(k).forEach((e) => cs(String(e)));
    // update state (keep only active pockets)
    for (const [k, v] of Object.entries(sp)) {
      if (v.n) sp[k] = { ...v, s: 1 };
      else delete sp[k];
    }
  }

  return Object.values(sp)
    .map((v) => v.s)
    .filter((v) => v > 0)
    .reduce(h.add);
};

const part1 = (input) => ss(h.asLines(input), "xyz");
const part2 = (input) => ss(h.asLines(input), "xyzw");

module.exports = [part1, part2];
