const h = require("../tools/helpers");

const part1 = (input) => {
  const ii = h.asNums(input);
  let pc = 1; let ec = 1;
  while (pc != ii[0]) {
    pc = (7 * pc) % 20201227;
    ec = (ii[1] * ec) % 20201227;
  }
  return ec;
};

module.exports = [part1];
