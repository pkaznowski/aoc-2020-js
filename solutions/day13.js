const h = require("../tools/helpers");

const part1 = (input) => {
  let [tt, bb] = h.asLines(input);
  bb = bb.match(/\d+/g).map((n) => parseInt(n, 10));
  for (let i = tt; ; i++) for (b of bb) if (i % b == 0) return b * (i - tt);
};

const part2 = (input) => {
  const dd = h.asLines(input)[1].split(",");
  let bb = [];
  dd.forEach((b, i) => { if (!isNaN(b)) bb.push([parseInt(b, 10), i]) });

  const ss = (bb, time = 0, inc = 1) => {
    if (bb.length == 0) return time;
    const [[b, min], ...rest] = bb;
    return (min + time) % b == 0
      ? ss(rest, time, inc * b)
      : ss(bb, time + inc, inc);
  };

  return ss(bb);
};

module.exports = [part1, part2];
