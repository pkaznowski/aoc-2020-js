const h = require("../tools/helpers");

const axialGrid = (input) => {
  const ii = h.asLines(input).map((l) => l.match(/nw|sw|ne|se|e|w/g));
  const move = {
    e: (x, y) => [x + 1, y], se: (x, y) => [x, y + 1], sw: (x, y) => [x - 1, y + 1],
    w: (x, y) => [x - 1, y], nw: (x, y) => [x, y - 1], ne: (x, y) => [x + 1, y - 1],
  };
  return ii.reduce((AG, m) => {
    const tile = m.reduce((c, d) => move[d](...c.map((n) => parseInt(n, 10))), [0, 0]);
    AG[tile] = {co: tile, bl: tile in AG ? +!Boolean(AG[tile].bl) : 1};
    return AG;
  }, {});
};

const part1 = (input) => Object.values(axialGrid(input)).reduce((s, e) => s + e.bl, 0);

const part2 = (input) => {
  let AG = axialGrid(input);

  // day: get neighbors -> "old" and "new" (new need to be updated in the second run)
  const gn = ([x, y]) =>
    [[-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0]]
      .map(([xx, yy]) => [xx + x, yy + y])
      .reduce(([o, n], e) => (e in AG ? [[...o, e], n] : [o, [...n, e]]), [[], []]);

  for (let d = 0; d < 100; d++) {
    let NN = []; // newly discovered tiles
    // get state of old tiles and save discovered ones
    Object.entries(AG).forEach(([k, v]) => {
      let [on, nn] = gn(v.co);
      on = on.filter((n) => AG[n].bl).length;
      next = (v.bl && (on == 0 || on > 2)) ? 0 : (!v.bl && on == 2) ? 1 : v.bl;
      AG[k] = { ...v, next: next }; NN = [...NN, ...nn];
    });
    // get state of new tiles
    NN.forEach((k) => {
      const nn = gn(k)[0].filter((n) => AG[n].bl).length;
      AG[k] = { co: k, next: (nn == 2) ? 1 : 0 };
    });
    // update state
    Object.entries(AG).forEach(([k, v]) => (AG[k] = { ...v, bl: v.next }));
  }

  return Object.values(AG).reduce((s, e) => s + e.bl, 0);
};

module.exports = [part1, part2];
