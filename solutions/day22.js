const h = require("../tools/helpers");

const ss = (input, game, part) => {
  let decks = input.split(/Player .:/).filter(e => e).map(e => h.asNums(e))
  const winner = (part == 1) ? game(...decks) : game(...decks)[1]
  return winner.reduce(([i, s], c) => [i + 1, s + c * i], [1, 0])[1]
}

const part1 = (input) => {
  const game = (P1, P2) => {
    while (P1.length > 0 && P2.length > 0) {
      let [p1, p2] = [P1.shift(), P2.shift()]
      if (p1 > p2) P1.push(p1, p2); else P2.push(p2, p1)
    } return (P1.length > 0) ? P1.reverse() : P2.reverse()
  }

  return ss(input, game, 1)
};

const part2 = (input) => {
  const dump = (...dd) => dd.map(e => e.join(",")).join(";")

  const game = (P1, P2) => {
    let rounds = []
    while (P1.length > 0 && P2.length > 0) {
      if (rounds.includes(d = dump(P1, P2))) return [true, P1]
      rounds.push(d)
      let [p1, p2] = [P1.shift(), P2.shift()]
      const oneWon = (P1.length >= p1 && P2.length >= p2)
        ? game(P1.slice(0, p1), P2.slice(0, p2))[0]
        : (p1 > p2)
      if (oneWon) P1.push(p1, p2); else P2.push(p2, p1)
    }
    return [(p1 = P1.length > 0), p1 ? P1.reverse() : P2.reverse()] 
  }

  return ss(input, game, 2)
};

module.exports = [part1, part2];
