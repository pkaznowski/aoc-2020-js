const h = require("../tools/helpers");

const mm = (nn, tt, ff = 2) => {
  for (let i = 0; i < nn.length; i++) {
    if (ff > 2) {
      if ((rr = mm(nn.slice(i + 1), tt - nn[i], ff - 1))) return nn[i] * rr;
      continue;
    }
    if (nn.slice(i + 1).includes(tt - nn[i])) return nn[i] * (tt - nn[i]);
  }
};

const part1 = (input) => mm(h.asNums(input), 2020);
const part2 = (input) => mm(h.asNums(input), 2020, 3);

module.exports = [part1, part2];
