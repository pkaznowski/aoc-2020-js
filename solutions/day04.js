const ii = (i) => i.replace(/\n\n/g, "@@").replace(/\n/g, " ").split("@@"); 
const kk = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

const part1 = (input) => {
  return ii(input).filter((l) => kk.every((e) => l.includes(`${e}:`))).length;
};

const part2 = (input) => {
  const lh = (i, [lo, hi]) => i >= lo && i <= hi;
  const rr = {
    byr: (i) => lh(i, [1920, 2002]),
    iyr: (i) => lh(i, [2010, 2020]),
    eyr: (i) => lh(i, [2020, 2030]),
    hgt: (i) => (!(m = i.match(/^(\d+)(cm|in)$/))) ? false : lh(m[1], {cm: [150, 193], in: [59, 76]}[m[2]]),
    hcl: (i) => i.match(/^#[0-9a-f]{6}$/),
    ecl: (i) => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].includes(i),
    pid: (i) => i.match(/^[0-9]{9}$/),
    cid: (i) => true
  };
  return ii(input).filter((l) => kk.every((e) => l.includes(`${e}:`))
      && l.split(" ").map(e => e.split(":")).every(([k, v]) => rr[k](v))
  ).length
};

module.exports = [part1, part2];
