const h = require("../tools/helpers");

const slide = (ii, xx, yy = 1) => {
  let pos = xx;
  let rr = 0;
  ii.forEach((l, i) => {
    if (i % yy === 0 && i !== 0) {
      rr += +(l[pos % l.length] == "#");
      pos += xx;
    }
  });
  return rr;
};

const part1 = (input) => {
  return slide(h.asLines(input), 3);
};

const part2 = (input) => {
  return [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]
    .map(([x, y]) => slide(h.asLines(input), x, y))
    .reduce((a, b) => a * b);
};

module.exports = [part1, part2];
