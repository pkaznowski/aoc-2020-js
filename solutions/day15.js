const ss = (i, turn) => {
  const ii = i.trim().split(",") .map((n) => parseInt(n, 10));

  let nums = new Map(); ii.forEach((e, i) => nums.set(e, i + 1));
  let [ln, tt] = [ii[ii.length - 1], ii.length];

  while (tt < turn) {
    nn = nums.has(ln) ? tt - nums.get(ln) : 0;
    nums.set(ln, tt);
    ln = nn;
    tt += 1;
  }
  return ln;
}

const part1 = (input) => ss(input, 2020);
const part2 = (input) => ss(input, 30000000);

module.exports = [part1, part2];
