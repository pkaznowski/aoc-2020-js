const h = require("../tools/helpers");

const categoriesToRanges = (ii) =>
  ii
    .map((l) => l.split(/: /))
    .reduce((ob, [k, v]) => {
      ob[k.trim()] = v
        .split(/ or /)
        .map((e) => e.match(/\d+/g).map((n) => parseInt(n, 10)));
      return ob;
    }, {});

const isInRanges = (nn, ...ranges) =>
  Array.from(new Set(nn)).every((n) =>
    ranges.some((rr) => rr.some((r) => n >= r[0] && n <= r[1]))
  );

const parse = (input) => {
  let [cl, yt, nt] = input.trim().split(/\n\n/).map((l) => l.split(/\n/));
  cl = categoriesToRanges(cl);
  yt = yt[1].split(",").map((n) => parseInt(n, 10));
  nt = nt.slice(1).map((l) => l.match(/\d+/g).map((n) => parseInt(n, 10)));
  return [cl, yt, nt];
};

const part1 = (input) => {
  let [cl, _, nt] = parse(input);
  return nt.reduce((r, l) => {
    l.forEach((n) => { if (!isInRanges([n], ...Object.values(cl))) r += n });
    return r;
  }, 0);
};

const part2 = (input) => {
  let [cl, yt, nt] = parse(input);

  // get valid tickets and convert them into indexed 'columns'
  const cols = nt
    .filter((tt) => isInRanges(tt, ...Object.values(cl)))
    .concat([yt])
    .reduce((ob, l) => {
      l.forEach((n, i) => ob.set(i, ob.has(i) ? ob.get(i).add(n) : new Set([n])));
      return ob;
    }, new Map());

  // get possible categories for columns
  let colToCat = {};
  cols.forEach((c, i) => {
    Object.entries(cl).forEach(([cn, rng]) => {
      if (isInRanges(Array.from(c), rng))
        colToCat[i] = i in colToCat ? colToCat[i].add(cn) : new Set([cn]);
    });
  });

  // filter categories so each columns has only one
  let catToCol = {}; let idx = Object.keys(colToCat);

  while (idx.length > 0) {
    idx.forEach((i) => {
      if (colToCat[i].size == 1) {
        catToCol[Array.from(colToCat[i])] = i;
        idx = idx.filter((c) => c != i);
        idx.forEach((j) => colToCat[j] = h.difference(colToCat[j], colToCat[i]));
      }
    });
  }

  // get your ticket's departures multiplied
  return Object.keys(catToCol)
    .filter((k) => k.match(/departure/))
    .reduce((r, n) => r * yt[catToCol[n]], 1);
};

module.exports = [part1, part2];
