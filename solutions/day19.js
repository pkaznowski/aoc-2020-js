const h = require("../tools/helpers");

const mr = (r, R) =>
  /^[ab|]$/.test(r)
    ? r
    : "(" + r.split(" ").map((ch) => (ch == "|" ? ch : mr(R[ch], R))).join("") + ")";

const parse = (input) => {
  let [rr, msg] = h.groups(input);
  rr = rr
    .map((r) => r.split(/: /))
    .reduce((obj, [k, v]) => {
      obj[k] = /"/.test(v) ? v.match(/\w+/)[0] : v;
      return obj;
    }, {});
  return [rr, msg];
};

const part1 = (input) => {
  const [rr, msg] = parse(input);
  const re = RegExp("^" + mr(rr[0], rr) + "$");
  return msg.filter((s) => re.test(s)).length;
};

const part2 = (input) => {
  const [rr, msg] = parse(input);
  const [x, y] = [42, 31].map(n => RegExp("^" + mr(rr[n], rr)));

  let s = 0;
  for (l of msg) {
    cx = 0; cy = 0;
    while ((mx = l.match(x))) { cx += 1; l = l.slice(mx[0].length) };
    while ((my = l.match(y))) { cy += 1; l = l.slice(my[0].length) };
    if (cx > 0 && cy > 0 && cx > cy && l.length == 0) s += 1;
  }
  return s;
};

module.exports = [part1, part2];
