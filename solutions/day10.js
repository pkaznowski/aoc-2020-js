const h = require("../tools/helpers");

const part1 = (input) => {
  const ii = h.asNumsSorted(input);
  let [oo, tt] = [1, 1];
  ii.forEach((e, i) => {
    oo += +(e - ii[i - 1] == 1); tt += +(e - ii[i - 1] == 3);
  })
  return oo * tt;
};

const part2 = (input) => {
  const ii = h.asNumsSorted(input);
  ii.unshift(0);

  let nn = {};
  let ca = {};

  let aa = (l) =>
    l.length == 0
      ? 1
      : l.map((e) => (e in ca ? ca[e] : aa(nn[e]))).reduce(h.add);

  ii.forEach((e, i) => (nn[e] = ii.slice(i + 1, i + 4).filter(n => n - e <= 3)));
  Object.keys(nn).reverse().forEach((e) => (ca[e] = aa(nn[e])));

  return ca[0];
};

module.exports = [part1, part2];
