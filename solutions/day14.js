const h = require("../tools/helpers");

const zz = (n) => {
  const bi = parseInt(n).toString(2);
  return [...(new Array(36 - bi.length + 1).join("0") + bi)];
};

const ma = ([...m], n, c = "X") => {
  m.forEach((e, i) => {
    if (e != c) n[i] = e;
  });
  return c == "X" ? parseInt(n.join(""), 2) : n.join("");
};

const part1 = (input) => {
  const ii = h.asLines(input).map((e) => e.split(" = "));

  let mem = {};
  let mask;

  ii.forEach(([cc, vv]) => {
    if (cc == "mask") mask = vv;
    else mem[cc] = ma(mask, zz(vv));
  });

  return Object.values(mem).reduce(h.add);
};

const part2 = (input) => {
  const ii = h.asLines(input).map((e) => e.split(" = "));

  const ga = (c) => parseInt(c.match(/\d+/)[0]);
  const aa = (a) =>
    h
      .perm([1, 0], a.match(/X/g).length)
      .reduce(
        (re, x) => re.concat(x.reduce((w, n) => (w = w.replace("X", n)), a)),
        []
      );

  let mem = {};
  let mask;

  ii.forEach(([cc, vv]) => {
    if (cc == "mask") mask = vv;
    else aa(ma(mask, zz(ga(cc)), "0")).forEach((a) => (mem[a] = parseInt(vv)));
  });

  return Object.values(mem).reduce(h.add);
};

module.exports = [part1, part2];
