const h = require("../tools/helpers");

const pp = (i) => {
  return h.asLines(i).reduce((oo, l) => {
    const [ba, co] = l.split(" bags contain ");
    oo[ba] = co
      .split(/[,.]/)
      .filter((e) => e.length !== 0)
      .map((e) => e.trim().split(" "))
      .reduce((o, b) => {
        if (!isNaN(b[0])) o[b.slice(1, 3).join(" ")] = parseInt(b[0], 10);
        return o;
      }, {});
    return oo;
  }, {});
};

const part1 = (input) => {
  const ii = pp(input);
  let ca = {};

  const ss = (ob) => {
    if (Object.entries(ob).length == 0) return 0;
    for (let j of Object.keys(ob)) {
      if (ca.hasOwnProperty[j]) if (ca[j]) return 1; else continue;
      if ((j == "shiny gold") | (ca[j] = ss(ii[j]))) return 1;
    }
    return 0;
  };

  Object.keys(ii).forEach((k) => (ca[k] = ss(ii[k])));
  return Object.values(ca).reduce(h.add);
};

const part2 = (input) => {
  const ii = pp(input);
  let ca = {};

  const ss = (ob) => {
    return (Object.entries(ob).length == 0)
      ? 0
      : Object.keys(ob)
      .map((j) => ob[j] + ob[j] * (ca.hasOwnProperty[j] ? ca[j] : (ca[j] = ss(ii[j]))))
      .reduce(h.add);
  };

  return ss(ii["shiny gold"]);
};

module.exports = [part1, part2];
