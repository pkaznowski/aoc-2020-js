const h = require("../tools/helpers");
const ii = (i) => h.asLines(i).map((l) => l.split(" "));

const part1 = (input) => {
  return ii(input).filter(([ru, le, pa]) => {
    const [lo, hi] = ru.split("-");
    const m = pa.split("").filter((c) => c == le[0]).length;
    return m >= lo && m <= hi;
  }).length;
};

const part2 = (input) => {
  return ii(input).filter(
    ([ru, le, pa]) =>
      ru
        .split("-")
        .map((i) => pa.split("")[i - 1])
        .filter((c) => c == le[0]).length === 1
  ).length;
};

module.exports = [part1, part2];
