const h = require("../tools/helpers");

const sol = (s, f) => (m = s.match(/\(([^()]+)\)/)) 
  ? sol(s.slice(0, m.index) + f(m[1]) + s.slice(m[0].length + m.index), f)
  : f(s)

const part1 = (input) => {
  const math = (s) =>
    eval(
      s.split(" ").reduce((a, e) => {
        return a.length == 3 ? [eval(a.join("")), e] : [...a, e];
      }, []).join("")
    );

  return h.asLines(input).map((l) => sol(l, math)).reduce(h.add);
};

const part2 = (input) => {
  const math = (s) => s.split("*").map((e) => eval(e)).reduce(h.mul);
  return h.asLines(input).map((l) => sol(l, math)).reduce(h.add);
};

module.exports = [part1, part2];
