const [part1, part2] = require("../day23");

describe("day 23", () => {
  const ii = "389125467";
  describe("part 1", () => { it("", () => { expect(part1(ii)).toBe("67384529") })});
  describe("part 2", () => { it("", () => { expect(part2(ii)).toBe(149245887792) })});
});
