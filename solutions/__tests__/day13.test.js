const [part1, part2] = require("../day13");

describe("day 13", () => {
  const ii = '939\n7,13,x,x,59,x,31,19'
  describe("part 1", () => { it("", () => { expect(part1(ii)).toBe(295) })});
  describe("part 2", () => { it("", () => { expect(part2(ii)).toBe(1068781) })});
});
