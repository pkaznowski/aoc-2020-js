const [part1, part2] = require("../day17");

describe("day 17", () => {
  const ii = `\
.#.
..#
###`;
  describe("part 1", () => {
    it("", () => {
      expect(part1(ii)).toBe(112);
    });
  });
  describe("part 2", () => {
    it("", () => {
      expect(part2(ii)).toBe(848);
    });
  });
});
