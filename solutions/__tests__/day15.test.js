const [part1] = require("../day15");
describe("day 15", () => {
  describe("part 1", () => {
    it.each([
      ["0,3,6", 436],
      ["1,3,2", 1],
      ["2,1,3", 10],
      ["1,2,3", 27],
      ["2,3,1", 78],
      ["3,2,1", 438],
      ["3,1,2", 1836],
    ])("%s should be %i", (i, e) => { expect(part1(i)).toBe(e); }); });
});
