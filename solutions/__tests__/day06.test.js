const [part1, part2] = require("../day06");

describe("day 06", () => {
  const input = "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb"

  describe("solution for", () => {
    it("part 1", () => { expect(part1(input)).toBe(11); });
  });

  describe("solution for", () => {
    it("part 2", () => { expect(part2(input)).toBe(6); });
  });
});
