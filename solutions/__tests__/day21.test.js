const [part1, part2] = require("../day21");

describe("day 21", () => {
  const i = `\
mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)`;

  describe("part 1", () => { it("", () => { expect(part1(i)).toBe(5); }); });
  describe("part 2", () => { it("", () => { expect(part2(i)).toBe("mxmxvkd,sqjhc,fvjkl"); }); });
});
