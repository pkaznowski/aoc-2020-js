const [part1, part2] = require("../day09");

describe("day 09", () => {
  const ii = `\
35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576`;
  describe("part 1", () => { it("", () => { expect(part1(ii, 5)).toBe(127); }); });
  describe("part 2", () => { it("", () => { expect(part2(ii, 127)).toBe(62); }); });
});
