const [part1, part2] = require("../day01");

describe("day 01", () => {
  const data = "1721\n979\n366\n299\n675\n1456";

  describe("part 1", () => {
    it("checking two terms multiplication", () => {
      expect(part1(data)).toBe(514579);
    });
  });

  describe("part 2", () => {
    it("checking three terms multiplication", () => {
      expect(part2(data)).toBe(241861950);
    });
  });
});
