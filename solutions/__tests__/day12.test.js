const [part1, part2] = require("../day12");

describe("day 12", () => {
  const ii = `\
F10
N3
F7
R90
F11`;
  describe("part 1", () => { it("", () => { expect(part1(ii)).toBe(25)  })});
  describe("part 2", () => { it("", () => { expect(part2(ii)).toBe(286) })});
});
