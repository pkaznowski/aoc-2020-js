const [part1, part2] = require("../day11");

describe("day 11", () => {
  const ii = `\
L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`;

  describe("part 1", () => {
    it("", () => {
      expect(part1(ii)).toBe(37);
    });
  });
  describe("part 2", () => {
    it("", () => {
      expect(part2(ii)).toBe(26);
    });
  });
});
