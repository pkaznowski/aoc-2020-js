const [part1, part2] = require("../day02");

describe("day 02", () => {
  describe("solutions", () => {
    const input = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc\n";
    it("for part 1", () => {
      expect(part1(input)).toBe(2);
    });
  });

  describe("solutions", () => {
    const input = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc\n";
    it("for part 2", () => {
      expect(part2(input)).toBe(1);
    });
  });
});
