const [part1, part2] = require("../day10");

describe("day 10", () => {
  const ii = `\
16
10
15
5
1
11
7
19
6
12
4`;
  const jj = `\
28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3`;
  describe("part 1", () => {
    it.each([
      [ii, 35],
      [jj, 220],
    ])("", (d, e) => {
      expect(part1(d)).toBe(e);
    });
  });
  describe("part 2", () => {
    it.each([
      [ii, 8],
      [jj, 19208],
    ])("", (i, e) => {
      expect(part2(i)).toBe(e);
    });
  });
});
