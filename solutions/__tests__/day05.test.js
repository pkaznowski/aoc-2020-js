const [part1, part2] = require("../day05");

describe("day 05", () => {
  describe("solutions for part 1", () => {
    it.each([
      ["FBFBBFFRLR", 357],
      ["BFFFBBFRRR", 567],
      ["FFFBBBFRRR", 119],
      ["BBFFBBFRLL", 820],
    ])("%s should be %i", (i, r) => {
      expect(part1(i)).toBe(r);
    });
  });
});
