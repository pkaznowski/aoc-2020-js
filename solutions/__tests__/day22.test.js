const [part1, part2] = require("../day22");

describe("day 22", () => {
  const input = `\
Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10`;
  describe("part 1", () => { it("", () => { expect(part1(input)).toBe(306); }); });
  describe("part 2", () => { it("", () => { expect(part2(input)).toBe(291); }); }); 
});
