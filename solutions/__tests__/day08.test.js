const [part1, part2] = require("../day08");

describe("day 08", () => {
    const d = `\
nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`;
  describe("part 1", () => { it("", () => { expect(part1(d)).toBe(5); }); });
  describe("part 2", () => { it("", () => { expect(part2(d)).toBe(8); }); });
});
