const [part1, part2] = require("../day14");

describe("day 14", () => {
  const ii = `\
mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0`;
  const jj = `\
mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1`

  describe("part 1", () => { it("", () => { expect(part1(ii)).toBe(165); }); });
  describe("part 2", () => { it("", () => { expect(part2(jj)).toBe(208); }); });
});
