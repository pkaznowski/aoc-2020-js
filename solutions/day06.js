const h = require("../tools/helpers");

const part1 = (input) => {
  return input
    .split(/\n\n/)
    .map((g) => new Set(g.replace(/\n/g, "")))
    .map((s) => s.size)
    .reduce(h.add);
};

const part2 = (input) => {
  return input
    .split(/\n\n/)
    .map((g) => g.split(/\n/).map((p) => new Set(p)))
    .map((ss) => h.intersection(...ss).size)
    .reduce(h.add);
};

module.exports = [part1, part2];
