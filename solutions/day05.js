const h = require("../tools/helpers");

const ff = (ll, ch) => {
  if (ll.length === 1) return ll[0];
  const nl = ch.shift().match(/[FL]/)
    ? ll.slice(0, ll.length / 2)
    : ll.slice(ll.length / 2);
  return ff(nl, ch);
};

const ids = (input) => {
  const ii = h.asLines(input).map((l) => [...l]);
  const [rr, cc] = [128, 8].map((n) => [...Array(n).keys()]);
  return ii.map((l) => 8 * ff(rr, l.slice(0, 7)) + ff(cc, l.slice(7)));
};

const part1 = (input) => Math.max(...ids(input));

const part2 = (input) => {
  const ii = ids(input).sort(h.lt);
  for (let i = 1; i < ii.length; i++) {
    if (ii[i - 1] + 1 !== ii[i]) return ii[i - 1] + 1;
  }
};

module.exports = [part1, part2];
