const h = require("../tools/helpers");

const ss = ([...l], n) => {
  for (let i = 0; i < l.length; i++) if (l.includes(n - l[i])) return true;
  return false;
};

const part1 = (input, pr = 25) => {
  const ii = h.asNums(input);
  for (let i = pr; i < ii.length; i++) {
    if (!ss(ii.slice(i - pr, i), ii[i])) return ii[i];
  }
};

const part2 = (input, mm = 50047984) => {
  const ii = h.asNums(input);

  for (let i = 0; i < ii.length; i++) {
    for (let j = i + 2; j < ii.length; j++) {
      const su = ii.slice(i, j);
      if (su.reduce(h.add) == mm) return Math.min(...su) + Math.max(...su);
    }
  }
};

module.exports = [part1, part2];
