const h = require("../tools/helpers");

const rr = {
  acc: (vv, ix, ac) => [ix + 1, ac + parseInt(vv)],
  jmp: (vv, ix, ac) => [ix + parseInt(vv), ac],
  nop: (_,  ix, ac) => [ix + 1, ac],
};

const part1 = (input) => {
  const ii = h.asLines(input).map((l) => l.split(" "));
  let [ac, ix, vi] = [0, 0, []]

  while (!vi.includes(ix)) {
    vi.push(ix); 
    [ix, ac] = rr[ii[ix][0]](ii[ix][1], ix, ac);
  }
  return ac;
};

const part2 = (input) => {
  const ii = h.asLines(input).map((l) => l.split(" "));
  let cc = [];
  ii.forEach((e, i) => { if (e[0] != "acc") cc.push(i); });

  for (let ij of cc) {
    let [ac, ix, vi] = [0, 0, []]

    while (!vi.includes(ix)) {
      vi.push(ix);
      op = (ix == ij) ? { nop: "jmp", jmp: "nop" }[ii[ij][0]] : ii[ix][0];
      [ix, ac] = rr[op](ii[ix][1], ix, ac);
      if (ix == ii.length - 1) return rr[ii[ix][0]](ii[ix][1], ix, ac)[1];
    }
  }
};

module.exports = [part1, part2];
