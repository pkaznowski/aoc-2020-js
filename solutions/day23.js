const h = require("../tools/helpers");

const part1 = (input) => {
  let cups = input.trim().split("").map((ch) => parseInt(ch, 10));
  let [cur, idx] = [cups[0], 0];

  for (t = 0; t < 100; t++) {
    let pu = [];
    while (pu.length < 3) {
      if (idx + 1 < cups.length) pu.push(...cups.splice(idx + 1, 1));
      else pu.push(cups.shift());
    }

    let dest = Math.max(...cups);
    for (c = cur; c > 0; c--) {
      if (cups.includes(c - 1)) { dest = c - 1; break; }
    }

    const dix = cups.indexOf(dest);
    cups.splice(dix + 1, 0, ...pu);
    cur = (m = cups.indexOf(cur) + 1) < cups.length ? cups[m] : cups[0];
    idx = cups.indexOf(cur);
  }

  return [
    ...cups.slice(cups.indexOf(1) + 1),
    ...cups.slice(0, cups.indexOf(1)),
  ].join("");
};

const part2 = (input) => {
  /* Will take advantage of array indices to order the cups
  element at index 3 will have value of the cup after the cup nr 3 etc.
  So for test input 389125467 the array will look like:

   value |  0   2   5   8   6   4   7  10   9   1   11  ... 1000000         3
   index | [0] [1] [2] [3] [4] [5] [6] [7] [8] [9] [10] ... [999999] [1000000]

  We don't care about the index 0 since we will never use it.
  Mind that index 7 points to the next integer after the max(input), since 7
  is the last number in the initial input; the last element in the array is
  3 as the first label in the input.
  Now, each move will require only 3 manipulations on the array. */

  const cups = input.trim().split("").map((ch) => parseInt(ch, 10));

  let ord = [...new Array(10).values()]
  cups.forEach((c, i) => ord[c] = i < cups.length - 1 ? cups[i + 1] : 10 )

  let succ = [...ord, ...[...new Array(1000001).keys()].slice(ord.length + 1)];
  succ[1000000] = cups[0]

  const move = (cur) => {
    const a = succ[cur], b = succ[a], c = succ[b], next = succ[c]
    let dest = cur - 1

    while (true) {
      if (dest < 1) dest = 1000000
      if (![a, b, c].includes(dest)) break
      dest -= 1
    }

    succ[cur] = next, succ[c] = succ[dest], succ[dest] = a
    return next
  }

  let cur = cups[0]
  for (let i = 0; i < 10000000; i++) cur = move(cur)

  return succ[1] * succ[succ[1]]
};

module.exports = [part1, part2];
