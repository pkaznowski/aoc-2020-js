const h = require("../tools/helpers");

const ur = {
  N: (d, v) => ({ ...d, y: d.y + v }),
  S: (d, v) => ({ ...d, y: d.y - v }),
  E: (d, v) => ({ ...d, x: d.x + v }),
  W: (d, v) => ({ ...d, x: d.x - v }),
};

const part1 = (input) => {
  const rr = {
    ...ur,
    R: (d, v) => ({ ...d, f: (c = (d.f + v) % 360) < 0 ? 360 + c : c }),
    L: (d, v) => ({ ...d, f: (c = (d.f - v) % 360) < 0 ? 360 + c : c }),
  };

  const ff = (f, d) =>
    f == "F" ? { 0: "N", 90: "E", 180: "S", 270: "W" }[d] : f;

  const ss = h
    .asLines(input)
    .map((m) => [m[0], parseInt(m.slice(1))])
    .reduce((dd, l) => rr[ff(l[0], dd.f)](dd, l[1]), { f: 90, x: 0, y: 0 });

  return Math.abs(ss.x) + Math.abs(ss.y);
};

const part2 = (input) => {
  const ii = h.asLines(input).map((m) => [m[0], parseInt(m.slice(1))]);

  const rr = {
    ...ur,
    R: (d, v) => [d, {x: d.y, y: -d.x}, {x: -d.x, y: -d.y}, {x: -d.y, y: d.x}][(v / 90) % 4],
    L: (d, v) => [d, {x: -d.y, y: d.x}, {x: -d.x, y: -d.y}, {x: d.y, y: -d.x}][(v / 90) % 4],
  };

  const ff = (s, w, v) => ({ x: s.x + v * w.x, y: s.y + v * w.y });

  const ss = ii.reduce((xy, [d, v]) => { 
    return (d == "F") 
      ? {...xy, s: ff(xy.s, xy.w, v)} 
      : {...xy, w: rr[d](xy.w, v)} 
  }, {w: {x: 10, y: 1}, s: {x: 0, y: 0}});

  return Object.values(ss.s).map(Math.abs).reduce(h.add);
};

module.exports = [part1, part2];
