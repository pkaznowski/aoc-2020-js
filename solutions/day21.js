const h = require("../tools/helpers");

const ss = (input, part) => {
  // parse
  const ii = h
    .asLines(input)
    .map((l) => l.replace(/\)|,/g, ""))
    .map((l) => l.split("(contains ").map((e) => new Set(e.trim().split(" "))));

  // get all alergens
  const aa = ii.reduce((s, e) => { e[1].forEach(s.add, s); return s; }, new Set());

  // and assign each of them possible matches
  let ALG = {};
  aa.forEach((a) => {
    ii.forEach(([ing, al]) => {
      if (al.has(a)) ALG[a] = (a in ALG) ? h.intersection(ALG[a], ing) : ing;
    });
  });

  // part 1
  if (part == 1) {
    // merge all possible alergens
    let A = new Set(); for (v of Object.values(ALG)) v.forEach(A.add, A);
    // get differences between possible alergens and other ingredients
    return ii.map((e) => e[0]).map((v) => h.difference(v, A).size).reduce(h.add);
  }

  // part 2
  let f = [];
  while (f.length < Object.keys(ALG).length) {
    for ([a, av] of Object.entries(ALG)) {
      if (av.size == 1 && !f.includes(a)) {
        for ([o, ov] of Object.entries(ALG)) 
          ALG[o] = (a != o) ? h.difference(ov, av) : ALG[o]
        f.push(a); }}}

  f.sort();
  return f.map((a) => Array.from(ALG[a])[0]).join(",");
};

const part1 = (input) => ss(input, 1); 
const part2 = (input) => ss(input, 2); 

module.exports = [part1, part2];
