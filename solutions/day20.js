const h = require("../tools/helpers");
const rev = (s) => s.split("").reverse().join("");

class Tile {
  constructor(lines, part1 = true) {
    const _rows = part1 ? lines.slice(1) : lines
    this.nr = part1 ? parseInt(lines[0].match(/\d+/)[0], 10) : undefined;
    this.XX = _rows[0].length;
    this.YY = _rows.length;
    this.gg = _rows.reduce(
      ([ob, y], l) => {
        l = part1 ? l.split("") : l;
        l.forEach((ch, x) => (ob[[x, y]] = ch));
        return [ob, y + 1] }, [{}, 0])[0];
    this.match = {};
  }

  get rows() {
    let r = [];
    for (let y = 0; y < this.YY; y++) {
      let l = [];
      for (let x = 0; x < this.XX; x++) {
        l.push(this.gg[[x, y]]);
      } r.push(l);
    } return r;
  }

  sides(all = false) {
    const s = ["ab", "ac", "bd", "cd"];
    const ss = all ? s.concat(s.map(rev)) : s;
    return ss.reduce((ob, p) => {
      const k = s.includes(p) ? [p, false] : [rev(p), true];
      ob[this._side(...k)] = p;
      return ob }, {})}

  _side(ft, revert = false) {
    const [from, to] = ft.split("");
    const max = this.XX - 1;
    const vertices = { a: [0, 0], b: [max, 0], c: [0, max], d: [max, max] };
    const re = ["ab", "cd"].includes(ft)
      ? RegExp(".," + vertices[to][1])
      : RegExp(vertices[from][0] + ",.");
    const chars = Object.keys(this.gg)
      .filter((k) => k.match(re))
      .map((k) => this.gg[k]);
    return revert ? chars.reverse().join("") : chars.join("")}

  _crd(type) {
    let C = [];
    for (let x = 0; x < this.XX; x++) {
      let col = [];
      for (let y = 0; y < this.YY; y++) {
        type == "cols" ? col.push([x, y]) : col.push([y, x]);
      } C.push(col);
    } return C }

  transform(...dir) {
    dir.forEach((d) => {
      if (d != "") {
        const ncrd = {
          H: this._crd("rows").reverse(),               // horizontal
          L: this._crd("cols").reverse(),               // rotate left
          R: this._crd("cols").map((c) => c.reverse()), // rotate right
          V: this._crd("rows").map((r) => r.reverse()), // vertical
        }[d];

        this.gg = ncrd
          .reduce((ar, r) => [...ar, ...r], [])
          .reduce(([ob, XY], xy) => {
            ob[XY.shift()] = this.gg[xy];
            return [ob, XY] }, [{}, Object.keys(this.gg)])[0] }});
  }

  show() { // for debugging only
    for (let y = 0; y < this.XX; y++) {
      for (let x = 0; x < this.XX; x++) process.stdout.write(this.gg[[x, y]]);
      console.log() }}}

const prepareTiles = (input) => {
  const ii = h.groups(input);

  let GG = ii.reduce((ob, t) => {
    nt = new Tile(t);
    ob[nt.nr] = nt;
    return ob }, {});

  Object.values(GG).forEach((vv) => {
    Object.values(GG).forEach((oo) => {
      if (oo.nr != vv.nr) {
        const vvs = vv.sides(),
          oos = oo.sides(true),
          sv = new Set(Object.keys(vvs)),
          so = new Set(Object.keys(oos));
        if ((is = h.intersection(sv, so)).size > 0) {
          for (s of Array.from(is)) vv.match[vvs[s]] = [oo.nr, oos[s]] }}})});

  return GG;
};

const part1 = (input) => {
  // 1549 2693 3539 3709
  // UL: 3539
  const GG = prepareTiles(input);
  return Object.values(GG).reduce((m, v) => {
    if (Object.values(v.match).length == 2) m *= v.nr;
    return m }, 1);
};

const part2 = (input) => {
  const GG = prepareTiles(input);

  // * helpers
  // return next tile nr, necessary rotations, new CD and new BD
  // we will walk from upper left downwards, then right, downwards again etc.
  // A   B     A   B
  //        .>
  // C   D  .  C   D
  //   .    .
  //   .    .
  //   v    .
  // A   B  .
  //      ...
  // C   D

  const transformations = (match, dir, s1) => {
    ms = s1 in match ? s1 : rev(s1);
    [nr, s2] = match[ms];
    s2 = ["ab", "bd", "ac", "cd"].includes(s1) ? s2 : rev(s2);

    const down = {
      ab: [[],         { d: "cd" }],  // we don't care about the right side here
      ac: [["R", "V"], { d: "bd" }],
      ba: [["V"],      { d: "dc" }],
      bd: [["L"],      { d: "ac" }],
      ca: [["R"],      { d: "db" }],
      cd: [["H"],      { d: "ab" }],
      db: [["L", "V"], { d: "ca" }],
      dc: [["H", "V"], { d: "ba" }],
    };
    const right = {
      ab: [["L", "H"], { d: "bd", r: "cd" }],
      ac: [[],         { d: "cd", r: "bd" }],
      ba: [["L"],      { d: "ac", r: "dc" }],
      bd: [["V"],      { d: "dc", r: "ac" }],
      ca: [["H"],      { d: "ab", r: "db" }],
      cd: [["R"],      { d: "db", r: "ab" }],
      db: [["V", "H"], { d: "ba", r: "ca" }],
      dc: [["R", "H"], { d: "ca", r: "ba" }],
    };
    return [nr, ...(dir == "D" ? down[s2] : right[s2])];
  };

  // connect tiles, and flip them
  // start with upper left (this part is hardcoded)
  cur = GG[3539];
  let NG = {};
  NG[[0, 0]] = cur.nr;
  let curBD = "bd", curCD = "cd";
  [next, mov, side = { d }] = transformations(cur.match, "D", curCD);
  first = cur;

  // transform tiles and make New Grid of tiles
  const LEN = 12;
  for (let x = 0; x < LEN; x++) {
    for (let y = 0; y < LEN; y++) {
      if (x == 0 && y == 0) continue;
      cur = GG[next]; NG[[x, y]] = cur.nr; cur.transform(...mov);

      if (y < LEN - 1) {
        [next, mov, side = { d }] = transformations(cur.match, "D", side.d);
      } else {
        if (x == LEN - 1) break;
        [next, mov, side = { d, r }] = transformations(first.match, "R", curBD);
        curBD = side.r; first = GG[next];
      }
    }
  }

  // create image (remove borders && glue)
  let lines = {};
  for (let y = 0; y < LEN; y++) {
    for (let x = 0; x < LEN; x++) {
      tile = GG[NG[[x, y]]];
      for (let yy = 0; yy < 10; yy++) {
        lines[[y, yy]] = lines[[y, yy]] || [];
        for (let xx = 0; xx < 10; xx++) {
          if (yy == 0 || xx == 0 || yy == 9 || xx == 9) continue;
          lines[[y, yy]].push(tile.gg[[xx, yy]]) }}}}

  let IMG = new Tile(Object.values(lines).filter((l) => l.length > 0), false);

  // find dragons
  const du = /^..................#./;
  const dm = /#....##....##....###/g;
  const dl = /^.#..#..#..#..#..#.../;

  let copy = IMG, count = 0;
  for (let v of ["", "V"]) {
    for (let h of ["", "H"]) {
      for (let r of ["", "R", "R", "R"]) {
        copy.transform(v, h, r);
        copy.rows.forEach((row, i) => {
          if ((match = row.join("").matchAll(dm))) {
            if (i != 0 || i != copy.YY) {
              for (m of match) {
                if (
                  copy.rows[i - 1].slice(m.index).join("").match(du) &&
                  copy.rows[i + 1].slice(m.index).join("").match(dl)
                ) count += 1; }}}});
        if (count) break }}
    copy = IMG
  }

  // count non-dragon hashes
  return (
    IMG.rows
      .reduce((acc, l) => [...acc, ...l], []).filter((e) => e == "#")
      .length - count * 15
  );
};

module.exports = [part1, part2];
