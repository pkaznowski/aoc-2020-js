// Basic arithetic operations and comparisons
const _sub = (a, b) => a - b;

exports.add = (a, b) => a + b;
exports.sub = _sub;
exports.mul = (a, b) => a * b;
exports.lt = _sub;
exports.gt = (a, b) => b - a;

// Sets
// > helpers.intersection({1,2,3}, {3,4,5}) => {3}
exports.intersection = (...SS) => {
  return SS.reduce((A, B) => {
    let X = new Set();
    B.forEach((v) => { if (A.has(v)) X.add(v); });
    return X;
  });
};

// > helpers.difference({1,2,3}, {3,4,5}) => {1,2}
exports.difference = (...SS) => {
  return SS.reduce((A, B) => {
    let X = new Set();
    A.forEach((v) => { if (!B.has(v)) X.add(v); });
    return X;
  });
};

// > helpers.symDifference({1,2,3}, {3,4,5}) => {1,2,4,5}
exports.symDifference = (...SS) => {
  return SS.reduce((A, B) => {
    let X = new Set();
    A.forEach((v) => { if (!B.has(v)) X.add(v); });
    B.forEach((v) => { if (!A.has(v)) X.add(v); });
    return X;
  });
};

// Collections
const _perm = (a, n) => {
  return n == 1
    ? a.map((e) => [e])
    : a.reduce((r, x) => [...r, ..._perm(a, n - 1).map((e) => [x, ...e])], []);
};
const _comb = (a, n) => {
  return n == 1
    ? a.reduce((r, e) => [...r, [e]], [])
    : a.reduce( (acc, e) => [...acc, ..._comb(a, n - 1).map((r) => [e, ...r])], []);
};

// Parsing input
const lines = (i) => i.split(/\n/).filter((l) => l.length !== 0);
const linesNums = (i) => lines(i).map((ch) => parseInt(ch, 10));
const linesNumsSorted = (i) => {
  let nums = linesNums(i);
  nums.sort(_sub);
  return nums;
};
const arrayNums = (i) => lines(i).map((l) => l.split(/\D/).map((n) => parseInt(n, 10)));
const groups = (i) => i.trim().split(/\n\n/).map((g) => g.split(/\n/));

exports.asLines = lines;
exports.asNums = linesNums;
exports.asNumsSorted = linesNumsSorted;
exports.asArrayNums = arrayNums;
exports.groups = groups;
exports.perm = _perm;
exports.comb = _comb;
