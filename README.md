[Advent of Code](https://adventofcode.com/) solutions in JavaScript
for 2020 (but may be used as a JS AoC framework in general).

# Installation & configuration

In the project root, run `npm i .`

Create `.env` file with contents:

    AOC_SESSION=XXXXX
    YEAR=XXXX

Where `AOC_SESSION` is your session cookie (to be found in request
headers for e.g. puzzle input page if you're logged into your AoC
account).

`YEAR` is optional -- if not specified, current year will be used.

## Solutions and tests

There are template files in `solutions` directory for one day
solutions and tests. Copy them adjusting numbers and filling the code.

    solutions
    ├── day00.js
    └── __tests__
        └── day00.test.js

## Helpers

You'll find basic helper functions in `tools/helpers.js`. 
Use `const h = require("../tools/helpers")` to import them.

## Commands

### Fetching input

Run `npm run in DAY`, where `DAY` is optional: if is not specified (or
is not a valid number in range 1..25) will default to `1`.

### Running tests

Run `npm test`. To run only new (i.e. not commited) tests, add
`-- -o`; to run only some tests use `-- -t <string>` to be matched
(e.g. "02" to run only tests for day 2).

### Solving a puzzle

Run `npm run out DAY PART`, where both arguments are optional: if
`DAY` is not specified will default to current day or -- if current
day is not in range 1..25 -- to `1`; if `PART` is not `1` or `2`, both
solutions will be run.

# Solutions

| puzzle                                                                               | notes                                                     |
| ------:                                                                              | ----                                                      |
| [Day 1](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day01.js)  |                                                           |
| [Day 2](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day02.js)  |                                                           |
| [Day 3](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day03.js)  | bools to numbers, forEach                                 |
| [Day 4](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day04.js)  | includes, every                                           |
| [Day 5](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day05.js)  | ranges                                                    |
| [Day 6](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day06.js)  | sets: intersection                                        |
| [Day 7](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day07.js)  | reducing data structures                                  |
| [Day 8](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day08.js)  | queue, caching                                            |
| [Day 9](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day09.js)  |                                                           |
| [Day 10](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day10.js) | tree, reverse, caching                                    |
| [Day 11](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day11.js) | 2D grid, combinations                                     |
| [Day 12](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day12.js) | manhattan                                                 |
| [Day 13](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day13.js) | modular math / periods, recursion                         |
| [Day 14](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day14.js) | bin 2 dec, permutations                                   |
| [Day 15](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day15.js) | Map! (vs Object => don't use it)                          |
| [Day 16](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day16.js) | criss cross, sets: difference                             |
| [Day 17](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day17.js) | 3D-4D grid, combinations; don't recur! (stack overflow)   |
| [Day 18](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day18.js) | regex                                                     |
| [Day 19](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day19.js) | regex                                                     |
| [Day 20](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day20.js) | crazy grid flipping                                       |
| [Day 21](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day21.js) | sets: difference                                          |
| [Day 22](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day22.js) | recursion, simple queue                                   |
| [Day 23](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day23.js) | big array ops                                             |
| [Day 24](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day24.js) | hexagonal grid                                            |
| [Day 25](https://gitlab.com/pkaznowski/aoc-2020-js/-/blob/master/solutions/day25.js) |                                                           |

